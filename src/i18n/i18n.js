import Vue from 'vue'
import VueI18n from 'vue-i18n'
import locale from 'element-ui/lib/locale';
import zhCN from './langs/zh-CN'
import zhHK from './langs/zh-HK'
import en from './langs/en'
import enLocale from 'element-ui/lib/locale/lang/en'
import zhCnLocale from 'element-ui/lib/locale/lang/zh-CN'
import zhHkLocale from 'element-ui/lib/locale/lang/zh-TW'



Vue.use(VueI18n)

const messages = {
    'en': Object.assign(en, enLocale),
    'zh-CN': Object.assign(zhCN, zhCnLocale),
    'zh-HK': Object.assign(zhHK, zhHkLocale)
}


const i18n = new VueI18n({
    locale: localStorage.getItem('locale') || 'zh-CN',
    messages
})


locale.i18n((key, value) => i18n.t(key, value)) //为了实现element插件的多语言切换

export default i18n