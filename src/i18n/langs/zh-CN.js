export default {
    login: {
        "EMAIL": "邮箱",
        "USERNAME": "用戶名",
        "PASSWORD": "密码",
        "SUBMIT": "提交",
        "RESET": "重置",
        "ERROR_EMPTY_PASSWORD": "密码不能为空",
        "INFO_LOGIN_SUCCESS": "登录成功",
        "A02": "密码错误",
        "SWITCHING_CLIENT": "正在切换客户，请稍候..."
    },
    header: {
        "CLICK_ON": "点击展开",
        "CLICK_ON_THE_FOLD": "点击收起",
        "USER_PROFILE": "用户配置",
        "CHANGE_PASSWORD": "更改密码",
        "CURRENT_PASSWORD": "当前密码",
        "NEW_PASSWORD": "新密码",
        "NEW_PASSWORD_AGAIN": "确认新密码",
        "UPDATE_PASSWORD": "更新密码",
        "MSG_ENTER_CURRENT_PASSWORD": "请输入当前密码",
        "MSG_ENTER_NEW_PASSWORD": "请输入新密码",
        "MSG_NEW_PASSWORD_NOT_MATCH": "新密码不匹配",
        "MSG_PASSWORD_UPDATED": "您的密码已成功更新，请使用新密码登录",
        "LOGOUT": "登出",
        "SWITCH_CLIENT": "切换客户",
        "SELECT_CLIENT_TO_SWITCH": "选择客户",
        "SWITCHED_TO_CLIENT": "切换为客户",
        "TASK_LIST": "任务列表",
        "FAILED_UPDATE_TASK_LIST": "更新任务列表失败",
        "CANCEL": "取消",
    },
    menu: {
        'CLIENT_LIST': '客户列表',
        "CS": "客服",
        'CS_ORDER_PUT': '批量修改订单',
        "CS_ORDER_LIST": "订单列表",
        "CS_REPORT_LIST": "订单报表",
        "USER_PERMISSION": "用户及权限",
        "ADMIN": "后台用户",
        "USER": "前台用户",
        "ROLE": "角色",
        "TABLE": "报表",
        "NEW_TABLE": '清关报表',
        "PERMISSION": "权限",
        "CUSTOMS_REPORT": "清关报表",
        "TAIL_REPORT": "末端报表",
    },
    cs: {
        'CANCEL': '取消',
        'DETERMINE': '确定',
        'COMPANY_NAME': '公司名称',
        'CLIENT_CODE': '客户编号',
        'OPERATION_SUCCESSFUL': "操作成功",
        'IS_INTERCEPT': '是否确定拦截',
        'INTERCEPT': '拦截',
        'CANCEL_INTERCEPTS': '解除拦截',
        'CANCEL_INTERCEPT': "是否取消拦截",
        'FILE_FORMAIS_T': '支持的文件格式',
        'SELECT_UPLOAD_FILE': '请选择需要上传的文件',
        'FORMAT_FILE': '格式文件',
        'OPERATION': '操作',
        'IMPORT_STATUS': '导入状态',
        'NUMBER_OF_FAILED_ROWS': '失败行数',
        'NUMBER_OF_SUCCESSFUL_LINES': '成功行数',
        'NUMBER_OF_PROCESSED_ITEMS': '已处理行数',
        'COUNTS': '总行数',
        'IMPORT_PROGRESS': '导入进度',
        'UPLOAD_NAME': '上传文档名',
        'UPLOAD_TEMP': '上传模板名',
        'COMPLETION_TIME': '完成时间',
        'SUBMISSION_TIME': '提交时间',
        'START_TIME': '开始时间',
        'REPORT_NAME': '报告名称',
        'CUSTOM_TRACK': '自定义轨迹',
        'PARCEL_DESTRUCTION': '包裹销毁',
        'PACKAGE_DAMAGED': '包装破损',
        'PACKAGE_LOST': "包裹丢失",
        'RETURN_TO_OVERSEAS_WAREHOUSE': '退回海外仓',
        'SIGNED_IN': '已签收',
        'RECIPIENT_REJECTS': '收件人拒收',
        'ABNORMAL_DELIVERY': '派送异常',
        'PACKAGE_ARRIVES_POINT': '包裹到达自提点',
        'DELIVERY_IN_PROGRESS': '派送中',
        'FIRST_DELIVERY': '首次派送',
        'DELIVERY_IN_TRANSIT': '派送转运中',
        'PARCEL_ARRIVES_SORTING_CENTER': "包裹到达分拣中心",
        "HAND_OVER_DELIVERY_COMPANY": '移交派送公司',
        "COMPLETE_EXPORT_DECLARATION": "完成出口报关",
        "OUTBOUND": "已出库",
        "COMPLETE_IMPORT_CUSTOMS_CLEARANCE": "完成进口清关",
        "FLIGHT_ARRIVES": "航班到达",
        "REPORT_TYPE": "报告类型",
        "FLIGHT_DEPARTURE": "航班起飞",
        "GENERATE_REPORT": "生成报告",
        "SUBMIT_TIME": "提交时间",
        "BY_ORDER_STATUS": "按订单状态",
        "REPORT_TIME_RANGE": "报告时间范围",
        "TIME_RANGE_FROM": "时间范围从",
        "TIME_RANGE_TO": "时间范围到",
        "EXPORT_PROGRESS": "导出进度",
        "EXPORT_STATUS": "导出状态",
        "DOWNLOAD_TMP": "下载模板",
        'DOWNLOAD': '下载',
        "HIDE": "隐藏",
        "REPORT_PARAMETERS": "报告参数",
        "TIME_RANGE": "时间范围",
        "START_TIME": "开始时间",
        "END_TIME": "结束时间",
        "PLEASE_SELECT": "请选择",
        "ORDER_CREATED": "订单资料接收",
        "WAREHOUSE_SCAN_IN": "已入库",
        "BAGGED_TO_DESTINATION": "已装袋/封箱",
        "SUBMIT": "提交",
        "CANCEL": "取消",
        "LAST_WEEK": "过去一星期",
        "LAST_MONTH": "过去一个月",
        "LAST_3_WEEK": "过去三个月",
        "ORDER_TRACK_AND_TRACE_REPORT": "订单追踪报告",
        "MESSAGE_TIME_RANGE_3_MONTHS": "时间范围最长为3个月"
    },
    table: {
        "BOX_LIST": "箱号列表",
        "REPORT_TYPE": "报表类型",
        "NUMBER_TYPE": "号码类型",
        "NEW_REPORT": "生成报表",
    },
    order: {
        "DELETEORDER_SELECTED": "批量删除 - 勾选的订单",
        "UNBLOACKORDER_SELECTED": "批量解除拦截 - 勾选的订单",
        "BLOACKORDER_SELECTED": "批量拦截 - 勾选的订单",
        "PACTHSTATUSSTATUS_EXCEL": "批量补货态 - Excel上传",
        "PACTHSTATUSSTATUS_SELECTED": "批量补货态 - 勾选的订单",
        "ORDERMANIFEST_SELECTED": "下载订单详细 - 勾选的订单",
        "ORDERMANIFEST_ALL": "下载订单详细 - 全部的订单",
        "ORDER_LIST_SELECTED": "下载订单摘要 - 勾选的订单",
        "ORDER_LIST_ALL": '下载订单摘要 - 全部的订单',
        "WAYBILL_NUMDER": '出库运单号',
        "MEAGE": "消息/信息",
        "LOCATION": "位置",
        "OK_MEDNT": '确定补录',
        "MEDNT_GOODS_STATUS": "补录货态",
        "OK_DELETE": "确定删除",
        "ERR_FILE": "错误上传",
        "OK_REMOVE": "确认拦截",
        "REMOVE_INTERCEPT": "确定解除拦截",
        "PAY": "已支付",
        "USERT_CLAIM": "客戶需求",
        "CUSTOMER": "客户",
        "REMARK": '备注',
        "PRODUCT": "产品",
        "RESTS": "其他",
        "STATUS": "根据状态",
        "TIME_STATUS": "状态时间",
        "SELECT": "搜查",
        "BATCH": "批量操作",
        'CANCEL': '取消',
        'DETERMINE': '确定',
        'WARNING': '警告',
        "ALL_LOGISTICS_PRODUCTS": "所有物流产品",
        "ALL_DESTINATION_COUNTRY": "所有目的国家",
        "ORDER_DATE": "下单日期",
        "LOGISTICS_PRODUCTION": "物流产品",
        "DESTINATION_COUNTRY": "目的国家",
        "STATUS_CATEGORY": "状态类",
        "RECEIVER_PHONE": "收件人电话",
        "STATUS_CODE": "货态",
        "OPERATION": "操作",
        "BATCH_APPROVAL": "批量审核",
        "BATCH_CANCELLATION": "批量取消",
        "BATCH_LABEL_PRINTING": "批量打印",
        "BATCH_LABEL_EXPORT": "批量下载面单",
        "INPUT_REFERENCE_MESSAGE": "请输入參考编号, 最多1000行",
        "INPUT_TRACKING_NUMBER_MESSAGE": "请输入追踪号码, 最多1000行",
        "BEFORE_APPROVAL": "审批前",
        "AFTER_APPROVAL": "审批后",
        "EDIT_ORDER": "修改订单",
        "ORDER_DETAIL": "订单详情",
        "ARE_YOU_SURE_MESSAGE": "你确定要离开?",
        "WARNING": "警告",
        "YES": "是",
        "NO": "否",
        "WAITING_CONFIRMED": "待审核",
        "WAITING_REVISED": "待修正",
        "WAITING_ORDERED": "待出单",
        "WAITING_INBOUND": "待交寄",
        "ON_DELIVERY": "运输中",
        "SIGNED": "已签收",
        "EXCEPTION": "异常订单",
        "BASIC_INFORMATION": "基本信息",
        "SENDER_INFORMATION": "寄件人信息",
        "RECEIVER_INFORMATION": "收件信息",
        "PRODUCT_INFORMATION": "货物信息",
        "VERIFIED_ALL": "全部订单",
        "TRACKING_NUMBER": "追踪号码",
        "FILTER": "过滤",
        "REFERENCE_NUMBER": "参考编号",
        "SHIPMENT_TYPE": "货物类型",
        "PRODUCT_CODE": "物流产品",
        "PAYMENT_METHOD": "付款方式",
        "SALE_PLATFORM": "产品销售平台",
        "DECLARED_VALUE": "申报价值",
        "TAX_TERM": "稅金支付",
        "COD_VALUE": "代收款(COD)",
        "PACKAGE_DIMENSIONS_CM": "包裹规格(CM)",
        "PACKAGE_WEIGHT_GRAM": "包裹重量(克)",
        "NUMBER_OF_PIECES": "件数",
        "SENDER_NAME": "寄件人姓名",
        "RECEIVER_NAME": "收件人姓名",
        "COMPANY_NAME": "公司名称",
        "ADDRESS": "地址",
        "CITY": "城市",
        "PHONE": "电话",
        "PROVINCE": "州/省",
        "EMAIL": "邮箱",
        "COUNTRY": "国家",
        "TAX_NUMBER": "税号",
        "POST_CODE": "邮编",
        "ENGLISH_PRODUCT_NAME": "英文品名",
        "CHINESE_PRODUCT_NAME": "中文品名",
        "PRODUCT_CATEGORY": "商品分类",
        "UNIT_PRICE": "单价",
        "QUANTITY": "数量",
        "SKU": "SKU",
        "HS_CODE": "HS码",
        "BRAND": "品牌",
        "COUNTRY_OF_ORIGIN": "原产地",
        "ADD_NEW_LINE": "增加一行",
        "SAVE_AS_DRAFT": "保存到待审核",
        "CREATE_SHIPMENT": "直接生成订单",
        "SAVE_AND_CREATE": "生成订单",
        "CANCEL": "取消",
        "PAYMENT_METHOD_PP": "預付",
        "PAYMENT_METHOD_COD": "貨到付款",
        "TAX_TERM_DDP": "完税后交货 (DDP)",
        "TAX_TERM_DDU": "未完税交货 (DDU)",
        "SHIPMENT_TYPE_GENERAL": "普通货",
        "SHIPMENT_TYPE_SENSITIVE": "敏感货"
    },
    "air": {
        "PAGE_TITLE": "仓库操作 -> 空运操作",
        "CREATE_MAWB": "新增 MAWB",
        "ACTION_REFRESH": "刷新",
        "PLEASE_SELECT": "请选择数值",
        "PLEASE_ENTER": "请输入数值",
        "ACTION_CREATE": "新增",
        "ACTION_CANCEL": "取消",
        "ACTION_RESET": "重设",
        "ACTION_UPDATE": "更新",
        "ACTION_ADD": "新增",
        "MAWB_NUMBER": "MAWB 号码",
        "AIRLINE": "航空公司",
        "ORIGIN_PORT": "出发地机场",
        "DESTINATION_PORT": "目的地机场",
        "BOXIDS": "箱号",
        "TRACKING_NUMBERS": "追踪编号",
        "UPDATE_TIMESTAMP": "更新时间戳",
        "UPDATE_PREADVICE": "更新预报",
        "TIMESTAMP": "时间戳",
        "FLIGHT_NUMBER": "航班编号",
        "ETD": "ETD",
        "ETA": "ETA",
        "GROSS_WEIGHT": "Gross Wt.",
        "CHARGEABLE_WEIGHT": "Chg. Wt.",
        "FREIGHT_COST": "Freight Cost",
        "FREIGHT_COST_CURRENCY": "Currency",
        "ADD_BOXES": "新增箱子",
        "ADD_EXCEPTION": "新增例外",
        "ITEM_TYPE": "输入类型",
        "EXCEPTION_TYPE": "例外类型",
        "EXCEPTION_LOCATION": "例外位置",
        "MAWB_INVALID": "MAWB 号码不正确",
        "PARCELS": "小包"
    },
    "admin": {
        "ACTION_CREATE": "新增后台用户",
        "ACTION_EDIT": "修改后台用户",
        "ACTION_DELETE": "刪除后台用户",
        "NAME": "名字",
        "USERNAME": "登入名称",
        "PASSWORD": "密码",
        "ROLES": "角色",
        "PERMISSIONS": "权限",
        "ADDITIONAL_PERMISSIONS": "追加权限",
    },
    "user": {
        "ACTION_CREATE": "新增后台用户",
        "ACTION_EDIT": "修改前台用户",
        "ACTION_DELETE": "刪除前台用户",
        "NAME": "名字",
        "USERNAME": "登入名称",
        "PASSWORD": "密码",
        "ROLES": "角色",
        "PERMISSIONS": "权限",
        "ADDITIONAL_PERMISSIONS": "追加权限",
    }
}