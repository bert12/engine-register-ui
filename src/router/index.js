import Vue from 'vue'
import Router from 'vue-router'
import register from '@/view/register'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            redirect: '/register'
        },
        {
            path: '/register',
            name: 'register',
            component: login => require(['@/view/register'], login)
        }
    ]
})